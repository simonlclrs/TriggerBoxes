// -------- LIBRARIES --------
#include <SPI.h>               // Communication
#include <RF24.h>              // Radio module
#include <nRF24L01.h>
#include <Wire.h>              // I2C Comm module
#include <Adafruit_GFX.h>      // Screen display
#include <Adafruit_SSD1306.h>  // Screen lib


// -------- SCREEN PARAMS ----------
#define SCREEN_WIDTH 128  // OLED display width
#define SCREEN_HEIGHT 32  // OLED display height
#define OLED_RESET    -1  // Reset pin # (or -1 if sharing Arduino reset pin)
// -------- SCREEN OBJECT ---------
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);


// -------- RADIO PARAMETERS --------
#define PIN_CE   7
#define PIN_CSN  8
#define RF_SPEED RF24_250KBPS    // Available speeds : RF24_250KBPS / RF24_1MBPS / RF24_2MBPS
#define RF_POWER RF24_PA_HIGH    // Available powers : RF24_PA_MIN  / RF24_PA_LOW / RF24_PA_HIGH / RF24_PA_MAX
// -------- RADIO OBJECT ------------
RF24 radio(PIN_CE, PIN_CSN);
#define CHANNEL 69               // Frequency channel, 0-125
const byte address[6] = "00007"; // Radio address


// -------- INPUT PINS --------
#define PIN_BP1 A2   // + button
#define PIN_BP2 9    // - button
#define PIN_VAL 3    // Valid button
#define PIN_BC1 6    // Control button 1
#define PIN_BC2 5    // Control button 2
#define PIN_BC3 4    // Control button 3
#define PIN_BC4 2    // Control button 4

// -------- OUTPUT PINS --------
#define PIN_LEDR A1  // Red led
#define PIN_LEDV A0  // Green led


// -------- INPUT VARIABLES --------
bool bp1 = 0;
bool bp2 = 0;
bool val = 0;
bool bc1 = 0;
bool bc2 = 0;
bool bc3 = 0;
bool bc4 = 0;


// -------- MODES VARIABLES --------
byte mode = 1;
// Mode 1 - Direct

// Mode 2 - Sequence
#define MODE2_INCREMENT 100
bool modif = false;
byte bpActif = 1;
unsigned int sequenceDelays[4] = {0, 0, 0, 0};

// Mode 3 - Delay after press
#define MODE3_INCREMENT 100
bool edit = false;
byte bpModif = 1;
unsigned int buttonsDelay[4] = {0, 0, 0, 0};

// Mode 4 - Turn on / off
bool activated[4] = {false, false, false, false};
#define RESET_CODE 200


// DEBUG
#define DEBUG 0


// -------- SETUP --------
void setup() {
  // Serial link - debug
  #if DEBUG
    Serial.begin(9600);
  #endif


  // INIT INPUT PINS
  pinMode(PIN_BP1, INPUT);
  pinMode(PIN_BP2, INPUT);
  pinMode(PIN_VAL, INPUT);
  pinMode(PIN_BC1, INPUT);
  pinMode(PIN_BC2, INPUT);
  pinMode(PIN_BC3, INPUT);
  pinMode(PIN_BC4, INPUT);

  // INIT OUTPUT PINS
  pinMode(PIN_LEDR, OUTPUT);
  pinMode(PIN_LEDV, OUTPUT);
  digitalWrite(PIN_LEDR, LOW);
  digitalWrite(PIN_LEDV, LOW);
  digitalWrite(PIN_BP1, LOW);


  // INIT SCREEN
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }
  display.display();
  delay(500);


  // INIT RADIO
  radio.begin();                         // Start communication
  radio.setChannel(CHANNEL);             // Transmission channel
  radio.setPALevel(RF_POWER);            // Power level
  radio.setDataRate(RF_SPEED);           // Transmission speed
  radio.openWritingPipe(address);        // Writing channel
  radio.stopListening();                 // Set in writing mode
  
  #if DEBUG
    Serial.println("Radio initiated");
  #endif


  // Display mode on screen
  displayMode(mode);

  // End of setup
  blinkGreen(250, 2);
}


// -------- LOOP --------
void loop() {
  if (mode == 1) {        // MODE DIRECT
    // Lecture valeurs boutons
    //readButtons();

    // ACTIONS BOUTONS COMMANDE
    if (digitalRead(PIN_BC1)) { // BP commande 1
      bcActiv(1);
      digitalWrite(PIN_LEDV, HIGH);
      displayButton(1);
      delay(350);
      displayMode(1);
      digitalWrite(PIN_LEDV, LOW);
    }
    if (digitalRead(PIN_BC2)) { // BP commande 2
      bcActiv(2);
      digitalWrite(PIN_LEDV, HIGH);
      displayButton(2);
      delay(350);
      displayMode(1);
      digitalWrite(PIN_LEDV, LOW);
    }
    if (digitalRead(PIN_BC3)) { // BP commande 3
      bcActiv(3);
      digitalWrite(PIN_LEDV, HIGH);
      displayButton(3);
      delay(350);
      displayMode(1);
      digitalWrite(PIN_LEDV, LOW);
    }
    if (digitalRead(PIN_BC4)) { // BP commande 4
      bcActiv(4);
      digitalWrite(PIN_LEDV, HIGH);
      displayButton(4);
      delay(300);
      displayMode(1);
      digitalWrite(PIN_LEDV, LOW);
    }

    // ACTION AUTRES BOUTONS
    if (digitalRead(PIN_BP1)) { // Bouton +
      mode = 2;

      displayMode(2);
      displayTime();
      delay(200);
    }
    if (digitalRead(PIN_BP2)) { // Bouton -
      mode = 4;

      displayMode(4);
      delay(200);
    }
  } else if (mode == 2) { // MODE SEQUENTIEL
    if (!modif) { // Séquence initialisée
      if (digitalRead(PIN_VAL)) { // Bouton valid
        // Activation modif sequence
        modif = true;

        // LED rouge allumée
        digitalWrite(PIN_LEDR, HIGH);

        // Affichage valeurs
        displayMode(2);
        displayTime();

        delay(200);
      } else if (digitalRead(PIN_BC1)) { // Bouton 1
        // LANCEMENT SEQUENCE
        launchSequence();
      }

      if (digitalRead(PIN_BP1)) { // Bouton +
        mode = 3;

        displayMode(3);
        delay(200);
      }

      if (digitalRead(PIN_BP2)) { // Bouton -
        mode = 1;

        displayMode(1);
        delay(200);
      }

    } else { // Modif séquence
      if (digitalRead(PIN_BP1)) { // Bouton +
        // Augmentation délai séquence
        sequenceDelays[bpActif - 1] += MODE2_INCREMENT;

        displayMode(2);
        displayTime();

        delay(150);
      }

      if (digitalRead(PIN_BP2)) { // Bouton -
        // Diminution délai séquence
        if (sequenceDelays[bpActif - 1] > 0) {
          sequenceDelays[bpActif - 1] -= MODE2_INCREMENT;

          displayMode(2);
          displayTime();
        }

        delay(150);
      }

      if (digitalRead(PIN_VAL)) { // Bouton valid
        if (bpActif < 4) { // Modif temps boutons
          bpActif += 1;

          displayMode(2);
          displayTime();

          delay(250);
        } else { // Dernier bouton fini
          modif = false;
          bpActif = 1;

          digitalWrite(PIN_LEDR, LOW);

          displayMode(2);
          displayTime();

          delay(300);
        }
      }

    }
  } else if (mode == 3) { // MODE DELAI
    if (!edit) { // Pret au lancement
      if (digitalRead(PIN_VAL)) { // Bouton valid
        // Activation modif sequence
        edit = true;

        // LED Rouge allumée
        digitalWrite(PIN_LEDR, HIGH);

        // Afficher valeurs
        displayMode(3);
        displayTimeMode3();
        
        delay(100);
      } else {
        if (digitalRead(PIN_BC1)) { // BP commande 1
          displayButton(1);
          bcDelay(1);
          delay(150);
          displayMode(3);
        }
        if (digitalRead(PIN_BC2)) { // BP commande 2
          displayButton(2);
          bcDelay(2);
          delay(150);
          displayMode(3);
        }
        if (digitalRead(PIN_BC3)) { // BP commande 3
          displayButton(3);
          bcDelay(3);
          delay(150);
          displayMode(3);
        }
        if (digitalRead(PIN_BC4)) { // BP commande 4
          displayButton(4);
          bcDelay(4);
          delay(150);
          displayMode(3);
        }

        if (digitalRead(PIN_BP1)) { // Bouton +
          mode = 4;

          displayMode(4);
          delay(200);
        }

        if (digitalRead(PIN_BP2)) { // Bouton -
          mode = 2;

          displayMode(2);
          displayTime();

          delay(200);
        }
      }
    } else { // Modification des délais
      if (digitalRead(PIN_BP1)) { // Bouton +
        // Augmentation délai séquence
        buttonsDelay[bpModif - 1] += MODE3_INCREMENT;

        displayMode(3);
        displayTimeMode3();

        delay(150);
      }

      if (digitalRead(PIN_BP2)) { // Bouton -
        // Diminution délai séquence
        if (buttonsDelay[bpModif - 1] > 0) {
          buttonsDelay[bpModif - 1] -= MODE3_INCREMENT;

          displayMode(3);
          displayTimeMode3();
        }
        delay(150);
      }

      if (digitalRead(PIN_VAL)) { // Bouton valid
        if (bpModif < 4) { // Modif temps boutons
          bpModif += 1;
          delay(100);
        } else { // Dernier bouton fini
          edit = false;
          delay(100);
        }
      }
    }
  } else if (mode == 4) { // MODE ACTIVATION
    // BOUTONS COMMANDE
    if(digitalRead(PIN_BC1)) { // BP commande 1
      bcSwitch(1);
      digitalWrite(PIN_LEDV, HIGH);
      displayButton(1);
      delay(350);
      displayMode(4);
      digitalWrite(PIN_LEDV, LOW);
    }
    if(digitalRead(PIN_BC2)) { // BP commande 2
      bcSwitch(2);
      digitalWrite(PIN_LEDV, HIGH);
      displayButton(2);
      delay(350);
      displayMode(4);
      digitalWrite(PIN_LEDV, LOW);
    }
    if(digitalRead(PIN_BC3)) { // BP commande 3
      bcSwitch(3);
      digitalWrite(PIN_LEDV, HIGH);
      displayButton(3);
      delay(350);
      displayMode(4);
      digitalWrite(PIN_LEDV, LOW);
    }
    if(digitalRead(PIN_BC4)) { // BP commande 4
      bcSwitch(4);
      digitalWrite(PIN_LEDV, HIGH);
      displayButton(4);
      delay(350);
      displayMode(4);
      digitalWrite(PIN_LEDV, LOW);
    }

    // ACTION AUTRES BOUTONS
    if (digitalRead(PIN_BP1)) { // Bouton +
      mode = 1;

      displayMode(1);
      sendReset();
      delay(200);
    }
    if (digitalRead(PIN_BP2)) { // Bouton -
      mode = 3;

      displayMode(3);
      sendReset();
      delay(200);
    }
  }


  // Fluidity delay
  delay(10);
}



// -------- READ BUTTONS VALUES --------
void readButtons() {
  bp1 = digitalRead(PIN_BP1);
  bp2 = digitalRead(PIN_BP2);
  val = digitalRead(PIN_VAL);
  bc1 = digitalRead(PIN_BC1);
  bc2 = digitalRead(PIN_BC2);
  bc3 = digitalRead(PIN_BC3);
  bc4 = digitalRead(PIN_BC4);

  #if DEBUG
    Serial.println("BP+ : " + String(bp1));
    Serial.println("BP- : " + String(bp2));
    Serial.println("VAL : " + String(val));
    Serial.println("BC1 : " + String(bc1));
    Serial.println("BC2 : " + String(bc2));
    Serial.println("BC3 : " + String(bc3));
    Serial.println("BC4 : " + String(bc4));
  #endif
}


// -------- SCREEN FUNCTIONS --------
void displayMode(int8_t mode) { // Top of the screen mode
  display.clearDisplay();

  display.setTextSize(1);                // Text size
  display.setTextColor(SSD1306_WHITE);   // Color
  display.setCursor(50, 0);              // Cursor in middle

  String txtMode = "Mode " + String(mode);
  display.println(txtMode);

  display.display();
  delay(20);
}

void displayButton(int8_t btn) { // Button pressed
  // No clear, displayed after screen clear
  display.setTextSize(2);                // Text size
  display.setTextColor(SSD1306_WHITE);   // Color

  // Display BC
  String txtBC = "SORTIE " + String(btn);
  display.setCursor(20, 18);
  display.println(txtBC);

  // Affichage
  display.display();
  delay(20);
}

void displayTime() {
  // No clear, displayed after screen clear
  display.setTextSize(1);                // Text size
  display.setTextColor(SSD1306_WHITE);   // Color

  // Display times
  for (byte i = 0; i < 4; i++) {
    String txtTps = String(sequenceDelays[i]);

    display.setCursor((i * 32) + (4 - txtTps.length()) * 5, 24); // Cursor pos i

    display.println(txtTps);
  }

  // If editing
  if (modif) {
    int8_t x = 32 * bpActif - 32;
    display.setCursor(x, 18);
    display.println("----");
  }

  // Display
  display.display();
  delay(20);
}

void displayTimeMode3() {
  // No clear, displayed after screen clear
  display.setTextSize(1);                // Text size
  display.setTextColor(SSD1306_WHITE);   // Color

  // Display times
  for (byte i = 0; i < 4; i++) {
    String txtTps = String(buttonsDelay[i]);

    display.setCursor((i * 32) + (4 - txtTps.length()) * 5, 24); // Cursor pos i

    display.println(txtTps);
  }

  // If editing
  if (edit) {
    int8_t x = 32 * bpModif - 32;
    display.setCursor(x, 18);
    display.println("----");
  }

  // Affichage
  display.display();
  delay(20);
}


// -------- RADIO FUNCTIONS --------
void bcActiv(byte bpAc) { // MODE 1
  int bpActiv = 10 + bpAc;
  radio.stopListening();
  radio.write(&bpActiv, sizeof(bpActiv)); // Send pressed
}

void bcDelay(byte bpAc) { // MODE 2
  digitalWrite(PIN_LEDV, HIGH);
  
  int bpActiv = 10 + bpAc;
  radio.stopListening();

  delay(buttonsDelay[bpAc - 1] - 10);
  radio.write(&bpActiv, sizeof(bpActiv)); // Send pressed button

  digitalWrite(PIN_LEDV, LOW);
}

void launchSequence() { // MODE 3
  digitalWrite(PIN_LEDV, HIGH);
  
  radio.stopListening();

  for (int i = 0; i < 4; i++) {
    int bpActiv = 10 + i + 1;

    delay(sequenceDelays[i] - 10);

    radio.write(&bpActiv, sizeof(bpActiv));
  }
  
  delay(500);
  digitalWrite(PIN_LEDV, LOW);
}

void bcSwitch(byte bpID) { // MODE 4
  int bpSend = 100 + bpID;
  radio.stopListening();
  radio.write(&bpSend, sizeof(bpSend));
}

void sendReset() { // Reset all outputs
  radio.stopListening();
  radio.write(&RESET_CODE, sizeof(RESET_CODE));
}


// -------- LEDs FUNCTIONS --------
void blinkRed(int interval, byte rep) {
  for (int i = 0; i < rep; i++) {
    digitalWrite(PIN_LEDR, HIGH);
    delay(interval);
    digitalWrite(PIN_LEDR, LOW);
    delay(interval);
  }
}

void blinkGreen(int interval, byte rep) {
  for (int i = 0; i < rep; i++) {
    digitalWrite(PIN_LEDV, HIGH);
    delay(interval);
    digitalWrite(PIN_LEDV, LOW);
    delay(interval);
  }
}
