// -------- LIBRARIES --------
#include <SPI.h>               // Communication
#include <RF24.h>              // Radio module
#include <nRF24L01.h>


// -------- RADIO PARAMETERS --------
#define PIN_CE   7
#define PIN_CSN  8
#define RF_SPEED RF24_250KBPS    // Available speeds : RF24_250KBPS / RF24_1MBPS / RF24_2MBPS
#define RF_POWER RF24_PA_HIGH    // Available powers : RF24_PA_MIN  / RF24_PA_LOW / RF24_PA_HIGH / RF24_PA_MAX
// -------- RADIO OBJECT ------------
RF24 radio(PIN_CE, PIN_CSN);
#define CHANNEL 69               // Frequency channel, 0-125
const byte address[6] = "00007"; // Radio address


// -------- OUTPUT PINS --------
#define PIN_LED1 A0  // Output led 1
#define PIN_LED2 A1  // Output led 2
#define PIN_LED3 A2  // Output led 3
#define PIN_LED4 A3  // Output led 4

#define PIN_COMM_1 2 // Output control 1
#define PIN_COMM_2 3 // Output control 2
#define PIN_COMM_3 4 // Output control 3
#define PIN_COMM_4 5 // Output v 4

#define PIN_BUZZER 9 // Buzzer on output


// -------- OUTPUT ARRAYS --------
int ledsPins[4] = {PIN_LED1, PIN_LED2, PIN_LED3, PIN_LED4};
int outputPins[4] = {PIN_COMM_1, PIN_COMM_2, PIN_COMM_3, PIN_COMM_4};


// -------- RADIO VARIABLES --------
char message[32];
#define RESET_CODE 200


// -------- OUTPUT VARIABLES -------
bool activ[4] = {false, false, false, false};
bool continuousActiv[4] = {false, false, false, false};
int outputDelays[4] = {0, 0, 0, 0};
#define TIME_ON 1000


// DEBUG
#define DEBUG 0


// -------- SETUP --------
void setup() {
  // Serial link - debug
  #if DEBUG
    Serial.begin(9600);
  #endif


  // INIT OUTPUT PINS
  pinMode(PIN_LED1, OUTPUT);
  pinMode(PIN_LED2, OUTPUT);
  pinMode(PIN_LED3, OUTPUT);
  pinMode(PIN_LED4, OUTPUT);
  digitalWrite(PIN_LED1, LOW);
  digitalWrite(PIN_LED2, LOW);
  digitalWrite(PIN_LED3, LOW);
  digitalWrite(PIN_LED4, LOW);
  digitalWrite(PIN_COMM_1, LOW);
  digitalWrite(PIN_COMM_2, LOW);
  digitalWrite(PIN_COMM_3, LOW);
  digitalWrite(PIN_COMM_4, LOW);
  digitalWrite(PIN_BUZZER, LOW);


  // INIT RADIO
  radio.begin();                         // Start communication
  radio.setChannel(CHANNEL);             // Transmission channel
  radio.setPALevel(RF_POWER);            // Power level
  radio.setDataRate(RF_SPEED);           // Transmission speed
  radio.openReadingPipe(0, address);     // Reading channel definition
  radio.startListening();                // Reading mode
  
  #if DEBUG
    Serial.println("Radio initiated");
  #endif


  // End of setup
  fullLed(500);
  buzzerActiv();
  delay(1000);
  fullLed(500);
  buzzerDesac();
}


// -------- LOOP --------
void loop() {
  if (radio.available()) { // Message available
    int msg = 0;
    radio.read(&msg, sizeof(msg));

    #if DEBUG
      Serial.println(msg);
    #endif
    
    if(msg >= 10 && msg < 20) { // Temporary activations
      byte bpAc = msg - 11;
  
      enableOutput(bpAc);
    } else if(msg > 100 && msg < 200) { // Turn on / off
      byte bpAc = msg - 101;

      continuousActiv[bpAc] = !continuousActiv[bpAc];

      switchSortie(bpAc);
    } else if(msg == RESET_CODE) { // Reset all outputs
      resetAll();
    }
  }


  // If output enabled
  if(activ[0] || activ[1] || activ[2] || activ[3]) {
    int t = millis();
    for(byte i = 0; i < 4; i++) {
      if(activ[i]) { // If output enabled
        if(t - outputDelays[i] > TIME_ON) { // Activate for at least defined time
          disableOutput(i);
          activ[i] = false;
        }
      }
    }
  }


  // Fluidity delay
  delay(5);
}


// -------- OUTPUT FUNCTIONS ---------
void enableOutput(byte outNumber) {
  // Turn on output
  digitalWrite(outputPins[outNumber], HIGH);
  // Turn on LED
  digitalWrite(ledsPins[outNumber], HIGH);
  // Buzzer activation
  buzzerActiv();

  // Activation variables
  activ[outNumber] = true;
  outputDelays[outNumber] = millis();
}

void disableOutput(byte numS) {
  // Turn off output
  digitalWrite(outputPins[numS], LOW);
  // Turn off led
  digitalWrite(ledsPins[numS], LOW);
  // Buzzer desactivation
  buzzerDesac();
  
  activ[numS] = false;
}

// Switching state
void switchSortie(byte outNumber) {
  // Output
  digitalWrite(outputPins[outNumber], continuousActiv[outNumber]);
  // LED
  digitalWrite(ledsPins[outNumber], continuousActiv[outNumber]);
}

// Reset all
void resetAll() {
  for(int i = 0; i < 4; i++) {
    continuousActiv[i] = false;
    digitalWrite(outputPins[i], LOW);
    digitalWrite(ledsPins[i], LOW);
  }
}


// -------- LED FUNCTIONS ---------
void fullLed(int dly) {
  digitalWrite(PIN_LED1, HIGH);
  digitalWrite(PIN_LED2, HIGH);
  digitalWrite(PIN_LED3, HIGH);
  digitalWrite(PIN_LED4, HIGH);

  delay(dly);

  digitalWrite(PIN_LED1, LOW);
  digitalWrite(PIN_LED2, LOW);
  digitalWrite(PIN_LED3, LOW);
  digitalWrite(PIN_LED4, LOW);
}


// -------- BUZZER FUNCTIONS ---------
void buzzerActiv() {
  digitalWrite(PIN_BUZZER, HIGH);
}

void buzzerDesac() {
  digitalWrite(PIN_BUZZER, LOW);
}
