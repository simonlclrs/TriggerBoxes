# TriggerBoxes
Arduino-controlled project to trigger pyrotechnic outputs remotely with NRF24L01 modules.

## Description
This project consists of 2 boxes, an emitter and a receiver, with 4 controlled outputs activated by a MOSFET circuit.\
It is first designed to trigger temporarily the outputs in order to light pyrotechnic fuses. But it can be used to other purposes.

### Transmitter
The transmitter box contains : 
- An Arduino NANO to control it,
- A battery,
- 4 trigger buttons (red !),
- 1 screen to display information,
- An NRF24L01 module to transmit data to the receiver.

Different modes are available, to activate manually each output, activate with a delay, to create a sequence or to turn on/off.\
The screen allows to display the current mode information, which button is pressed and the delay added.

### Receiver
The receiver box contains :
- An Arduino UNO,
- A battery,
- A MOSFET control circuit,
- 4 audio outlets to plug wires,
- LEDs to inform about outputs enabled,
- Security switches for each output,
- A power connector that will be connected to the outputs,
- A buzzer to alert of activation,
- An NRF24L01 module to receive data.

Each line possesses a security switch you have to turn on to enable the outputs when triggered and LEDs to display when security is disabled and the output is triggered.\
The external power connector allow you to pass different voltages and power inputs to power the outputs when enabled.

### MOSFET control PCB
The custom PCB contains a power circuit to allow running high current outputs through the connectors.\
Using a small transistor, it can be controlled with low current, so it's ideal for an arduino output.

The circuit can also be done easily without a PCB, in a pierced board or not, it was only design for convenience and to take less space in the box.

You can find the GERBER files for a unique circuit or in a 2x2 frame in the *pcb/* folder.

### Specifications
With these communication modules, the range is theoretically of 1km. By testing it, we get about 200m in a space with obstacles and 400 to 500m in a clear space (and without wifi perturbations).\
The different modes allows personalisation of the sequence to launch fireworks or something else.


## 3D files
You can also find the 3D files in thingiverse here :\
https://www.thingiverse.com/thing:6830067

And you can edit them yourself in Fusion360 with the .f3d files in the *print/FusionFiles* folder.


## Content
The diffent parts of the project are organized like this :
- Find the Arduino codes in the *codes/* folder,
- Find the 3D files in the *print/* folder,
- Find the spice simulation files in the *simu/* folder,
- Schematics can be found in the *schemas/* folder,
- PCB Gerber files for the MOSFET part is in the *pcb/* folder.
